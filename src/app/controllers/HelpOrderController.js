import { addHours } from 'date-fns';
import { Op } from 'sequelize';
import HelpOrder from '../models/HelpOrder';
import Registration from '../models/Registration';
import Student from '../models/Student';
import Mail from '../../lib/Mail';

class HelpOrderController {
  async store(req, res) {
    const { id: studentId } = req.params;

    const student = await Student.findByPk(studentId);

    if (!student) {
      return res.status(401).json({ error: 'Student registrations not found' });
    }

    const today = addHours(new Date(), 3);

    const registration = await Registration.findOne({
      where: {
        canceled_at: null,
        student_id: studentId,
        end_date: { [Op.gte]: today },
      },
    });

    if (!registration) {
      return res
        .status(401)
        .json({ error: 'Student has not valid registration' });
    }

    const { id, question, student_id } = await HelpOrder.create({
      student_id: studentId,
      ...req.body,
    });

    return res.json({ id, question, student_id });
  }

  async index(req, res) {
    const { id: studentId } = req.params;

    if (studentId) {
      const student = await Student.findByPk(studentId);

      if (!student) {
        return res
          .status(401)
          .json({ error: 'Student registrations not found' });
      }

      const today = addHours(new Date(), 3);

      const registration = await Registration.findOne({
        where: {
          canceled_at: null,
          student_id: studentId,
          end_date: { [Op.gte]: today },
        },
      });

      if (!registration) {
        return res
          .status(401)
          .json({ error: 'Student has not valid registration' });
      }

      const helpOrders = await HelpOrder.findAll({
        where: { answer: null, student_id: studentId },
        attributes: ['id', 'question'],
        include: [
          {
            model: Student,
            as: 'student',
            attributes: ['id', 'name', 'email'],
          },
        ],
      });

      return res.json(helpOrders);
    }

    const helpOrders = await HelpOrder.findAll({
      where: { answer: null },
      attributes: ['id', 'question'],
      include: [
        {
          model: Student,
          as: 'student',
          attributes: ['id', 'name', 'email'],
        },
      ],
    });

    return res.json(helpOrders);
  }

  async update(req, res) {
    const { id } = req.params;
    const { answer } = req.body;

    const helpOrder = await HelpOrder.findByPk(id);

    if (!helpOrder) {
      return res.status(400).json({ error: 'Help order does not exist.' });
    }

    const student = await Student.findByPk(helpOrder.student_id);

    const answer_at = new Date();

    const helpOrderAnswered = await helpOrder.update({
      ...helpOrder,
      answer,
      answer_at,
    });

    const { question, student_id } = helpOrderAnswered;

    await Mail.sendMail({
      to: `${student.email} <${student.name}>`,
      subject: `Gympoint - Resposta do seu pedido de auxílio`,
      template: 'helporder-answer',
      context: {
        student: student.name,
        question: helpOrder.question,
        answer,
      },
    });

    return res.json({ id, question, answer, answer_at, student_id });
  }
}

export default new HelpOrderController();
