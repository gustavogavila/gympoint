import {
  isBefore,
  addMonths,
  parseISO,
  isWithinInterval,
  format,
} from 'date-fns';
import pt from 'date-fns/locale/pt';
import Mail from '../../lib/Mail';
import Registration from '../models/Registration';
import Student from '../models/Student';
import Plan from '../models/Plan';

class RegistrationController {
  async store(req, res) {
    const { student_id, plan_id, start_date } = req.body;

    const student = await Student.findByPk(student_id);
    const plan = await Plan.findByPk(plan_id);

    if (!student) {
      return res.status(400).json({ error: 'Student not found.' });
    }

    if (!plan) {
      return res.status(400).json({ error: 'Plan not found.' });
    }

    const dateReg = parseISO(start_date);

    if (isBefore(dateReg, new Date())) {
      return res.status(400).json({ error: 'Past dates are not permitted.' });
    }

    const existingRegistration = await Registration.findOne({
      where: { student_id, canceled_at: null },
    });

    if (existingRegistration) {
      const startDate = existingRegistration.start_date;
      const endDate = existingRegistration.end_date;
      const isActiveReg = isWithinInterval(dateReg, {
        start: startDate,
        end: endDate,
      });
      if (isActiveReg) {
        return res
          .status(400)
          .json({ error: 'This student already has an active registration.' });
      }
    }

    const duration = Number(plan.duration);
    const planPrice = Number(plan.price);

    const endDateCalc = addMonths(dateReg, duration);

    const totalPrice = duration * planPrice;

    const { end_date, price } = await Registration.create({
      ...req.body,
      end_date: endDateCalc,
      price: totalPrice,
    });

    await Mail.sendMail({
      to: `${student.email} <${student.name}>`,
      subject: `Gympoint - Sua matrícula foi realizada`,
      template: 'cancellation',
      context: {
        student: student.name,
        plan: plan.title,
        price: plan.price,
        date: format(endDateCalc, "dd' de ' MMMM ' de ' yyyy", { locale: pt }),
      },
    });

    return res.json({ student_id, plan_id, start_date, end_date, price });
  }

  async index(req, res) {
    const registrations = await Registration.findAll({
      where: { canceled_at: null },
      attributes: ['id', 'start_date', 'end_date', 'price'],
      include: [
        {
          model: Student,
          as: 'student',
          attributes: ['id', 'name', 'email'],
        },
        {
          model: Plan,
          as: 'plan',
          attributes: ['id', 'title', 'duration', 'price'],
        },
      ],
    });

    return res.json(registrations);
  }

  async update(req, res) {
    const { id } = req.params;
    const { plan_id, start_date } = req.body;

    const registration = await Registration.findOne({
      where: { id, canceled_at: null },
    });

    if (!registration) {
      return res.status(400).json({ error: 'Registration not found.' });
    }

    const plan = await Plan.findByPk(registration.plan_id);

    if (start_date) {
      const newStartDate = parseISO(start_date);

      if (isBefore(newStartDate, new Date())) {
        return res.status(400).json({ error: 'Past dates are not permitted.' });
      }

      const newEndDateCalc = addMonths(newStartDate, plan.duration);

      registration.start_date = newStartDate;
      registration.end_date = newEndDateCalc;
    }

    if (plan_id) {
      const newPlan = await Plan.findByPk(plan_id);
      if (!newPlan) {
        return res.status(400).json({ error: 'Plan not found.' });
      }
      const newPrice = newPlan.price;
      const newDuration = newPlan.duration;
      const newEndDateCalc = addMonths(registration.start_date, newDuration);

      registration.plan_id = plan_id;
      registration.price = Number(newPrice) * Number(newDuration);
      registration.end_date = newEndDateCalc;
    }

    const updatedRegistration = await registration.update({
      start_date: registration.start_date,
      end_date: registration.end_date,
      plan_id: registration.plan_id,
      price: registration.price,
    });

    return res.json(updatedRegistration);
  }

  async delete(req, res) {
    const registration = await Registration.findByPk(req.params.id);

    if (!registration) {
      return res
        .status(400)
        .json({ error: 'This registration does not exist' });
    }

    registration.update({ canceled_at: new Date() });

    return res.json();
  }
}

export default new RegistrationController();
