import { subDays, addHours } from 'date-fns';
import { Op } from 'sequelize';
import Checkin from '../models/Checkin';
import Student from '../models/Student';
import Registration from '../models/Registration';

class CheckinController {
  async store(req, res) {
    const { id } = req.params;

    const student = await Student.findByPk(id);

    if (!student) {
      return res.status(401).json({ error: 'Student registrations not found' });
    }

    const today = addHours(new Date(), 3);

    const registration = await Registration.findOne({
      where: {
        canceled_at: null,
        student_id: id,
        end_date: { [Op.gte]: today },
      },
    });

    if (!registration) {
      return res
        .status(401)
        .json({ error: 'Student has not valid registration' });
    }

    const date7DaysBefore = subDays(today, 7);

    const checkins = await Checkin.findAll({
      where: {
        student_id: id,
        created_at: {
          [Op.between]: [date7DaysBefore, today],
        },
      },
    });

    if (checkins && checkins.length > 4) {
      return res
        .status(401)
        .json({ error: 'Student exceeded 7-day checkin limit' });
    }

    const checkin = await Checkin.create({ student_id: id });

    return res.json(checkin);
  }

  async index(req, res) {
    const checkins = await Checkin.findAll({
      where: { student_id: req.params.id },
    });
    return res.json(checkins);
  }
}

export default new CheckinController();
